var clientesObtenidos;

function gestionClientes() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";

  (async function(){
    var responseJSON = await fetch(url);
    productosObtenidos =  await responseJSON.json();
    procesarClientes();
  })();

  // fetch(url)
  //   .then(function(response) {
  //     return response.json();
  //   })
  //   .then(function(myJson) {
  //     // console.log(myJson);
  //     console.table(myJson.value);
  //     // console.table(JSON.parse(request.responseText).value);
  //   });

// var request = new XMLHttpRequest();
//
// request.onreadystatechange = function(){
//   if (this.readyState == 4 && this.status == 200) {
//     console.log(request.responseText);
//   }
// }
//
// request.open("GET", url, true);
// request.send();

}

function procesarClientes(){
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");



  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  productosObtenidos.value.map(function(ele){
    // console.log(ele);

    var nuevaFila = document.createElement("tr");
    var imgBander = document.createElement("img");
    imgBander.classList.add("flag");
    console.log(ele.Country);
    if(ele.Country == "UK") ele.Country = "United-Kingdom";

    imgBander.src = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"+ ele.Country + ".png";

    // var td = document.createElement("td");
    // td.innerText = ele;


    genearTd(nuevaFila, ele.ContactName);
    genearTd(nuevaFila, ele.CompanyName);
    // genearTd(nuevaFila, ele.Country);
    nuevaFila.appendChild(imgBander);

    tbody.appendChild(nuevaFila);
    tabla.appendChild(tbody);
    tablaClientes.appendChild(tabla);
  })
  };

  function genearTd(nuevaFila, ele){
  var td = document.createElement("td");
  td.innerText = ele;
  nuevaFila.appendChild(td);
  }
