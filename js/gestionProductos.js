var productosObtenidos;

function gestionProductos() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";

(async function(){
  var responseJSON = await fetch(url);
  productosObtenidos =  await responseJSON.json();
  procesarProductos();
})()

  // fetch(url)
  //   .then(function(response) {
  //     return response.json();
  //   })
  //   .then(function(myJson) {
  //     console.table(myJson.value);
  //     productosObtenidos = myJson.value;
  //     procesarProductos();
  //   });

// var request = new XMLHttpRequest();
//
// request.onreadystatechange = function(){
//   if (this.readyState == 4 && this.status == 200) {
//     console.log(request.responseText);
//   }
// }
//
// request.open("GET", url, true);
// request.send();

}

function procesarProductos(){
  // var JSONProductos = JSON.parse(productosObtenidos);
  // alert(productosObtenidos[1]);

  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  productosObtenidos.value.map(function(ele){
    console.log(ele);
    var nuevaFila = document.createElement("tr");

    genearTd(nuevaFila, ele.ProductName);
    genearTd(nuevaFila, ele.UnitPrice);
    genearTd(nuevaFila, ele.UnitsInStock);


    tbody.appendChild(nuevaFila);
    tabla.appendChild(tbody);
    tablaProductos.appendChild(tabla);
  })
};

function genearTd(nuevaFila, ele){
  var td = document.createElement("td");
  td.innerText = ele;
  nuevaFila.appendChild(td);
}
