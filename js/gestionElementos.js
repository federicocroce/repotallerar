var productosObtenidos;

function gestionElementos(element, ElementHTML) {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/" + element;

(async function(){
  var responseJSON = await fetch(url);
  productosObtenidos =  await responseJSON.json();
  procesarElementos(element, ElementHTML);
})()

}

function procesarElementos(element, ElementHTML){
  // var JSONProductos = JSON.parse(productosObtenidos);
  // alert(productosObtenidos[1]);

  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  productosObtenidos.value.map(function(ele){
    console.log(ele);
    var nuevaFila = document.createElement("tr");

    genearTd(nuevaFila, ele.ProductName);
    genearTd(nuevaFila, ele.UnitPrice);
    genearTd(nuevaFila, ele.UnitsInStock);

    tbody.appendChild(nuevaFila);
    tabla.appendChild(tbody);
    ElementHTML.appendChild(tabla);
  })
};

function genearTd(nuevaFila, ele){
  var td = document.createElement("td");
  td.innerText = ele;
  nuevaFila.appendChild(td);
}
